﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bufo.Model;

namespace Bufo.ViewModel
{
    class EventTableString
    {
        public string BkNames { get; set; }
        public string Descriptions { get; set; }
        public string Players { get; set; }
        public string BkEvent { get; set; }
        //public string Tm { get; set; }
        //public string Tb { get; set; }
        //public string Coefficients { get; set; }

        public EventTableString(SportEvent sportEvent1, SportEvent sportEvent2)
        {
            BkNames = sportEvent1.BkName + Environment.NewLine + sportEvent2.BkName;
            Descriptions = sportEvent1.Description + " " + sportEvent1.PartOfMatchName + Environment.NewLine + sportEvent2.Description + sportEvent2.PartOfMatchName;
            Players = sportEvent1.Player1 + " - " + sportEvent1.Player2 + Environment.NewLine + sportEvent2.Player1 +
                      " - " + sportEvent2.Player2;
            var addCef1 = sportEvent1.AdditionalCef != null ? sportEvent1.AdditionalCef.Value.ToString() : "";
            var addCef2 = sportEvent2.AdditionalCef != null ? sportEvent2.AdditionalCef.Value.ToString() : "";
            var mainCef1 = sportEvent1.MainCef.BkEventName + " " + addCef1 + " " + sportEvent1.MainCef.Value;
            var mainCef2 = sportEvent2.MainCef.BkEventName + " " + addCef2 + " " + sportEvent2.MainCef.Value;
            BkEvent = mainCef1 + Environment.NewLine + mainCef2;
        }
    }
}
