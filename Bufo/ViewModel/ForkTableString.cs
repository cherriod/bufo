﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bufo.Model
{
    public class ForkTableString
    {
        public string BkNames { get; set; }
        public string Descriptions { get; set; }
        public string Players { get; set; }
        public string Amounts { get; set; }
        public string Events { get; set; }
        public string Coefficients { get; set; }
        public string Profits { get; set; }
        public string ProfitPercent { get; set; }
        public string Xpath1 { get; set; }
        public string Xpath2 { get; set; }

        public Fork Fork;
        public SportEvent SportEvent1;
        public SportEvent SportEvent2;


        //public ForkTableString() { }

        public ForkTableString(Fork fork, SportEvent sportEvent1, SportEvent sportEvent2)
        {
            Fork = fork;
            SportEvent1 = sportEvent1;
            SportEvent2 = sportEvent2;
            Amounts = fork.AmountPlayer1 + Environment.NewLine + fork.AmountPlayer2;
            BkNames = sportEvent1.BkName + Environment.NewLine + sportEvent2.BkName;
            Coefficients = fork.Coefficient1.Value + Environment.NewLine + fork.Coefficient2.Value;
            Descriptions = sportEvent1.Description + " " + sportEvent1.PartOfMatchName + Environment.NewLine + sportEvent2.Description + " " + sportEvent2.PartOfMatchName;

            //Events = fork.BkEvent1 + " " + (fork.BkEvent1Value == null ? "" : fork.BkEvent1Value.ToString()) + Environment.NewLine +
            //    fork.BkEvent2 + " " + (fork.BkEvent2Value == null ? "" : fork.BkEvent2Value.ToString());

            Events = fork.Coefficient1.BkEventName + " " +
                     (fork.AdditionalCef1 == null ? "" : fork.AdditionalCef1.Value.ToString()) + Environment.NewLine +
                     fork.Coefficient2.BkEventName + " " +
                     (fork.AdditionalCef2 == null ? "" : fork.AdditionalCef2.Value.ToString());

            Players = sportEvent1.Player1 + " - " + sportEvent1.Player2 + Environment.NewLine + 
                sportEvent2.Player1 + " - " + sportEvent2.Player2;

            ProfitPercent = fork.ProfitPercent.ToString();
            Profits = fork.Profit1 + Environment.NewLine + fork.Profit2;
            Xpath1 = fork.Coefficient1.Xpath;
            Xpath2 = fork.Coefficient2.Xpath;

        }

        //private Fork fork { get; set; }
    }
}
