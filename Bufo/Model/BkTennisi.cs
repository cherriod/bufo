﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;

namespace Bufo.Model
{
    class BkTennisi
    {
        private Dictionary<BkEvent, int> _columnNumbersByBkEvents;
       

        readonly List<string> _matchPartKinds = new List<string>()
                {
                    "1", "2", "3", "4"
                };

        private string _player1 = "";
        private string _player2 = "";
        private string _name = "";



        private List<SportEvent> ParsePinkRow(HtmlNode trEvent, HtmlNode headerOfTable)
        {
            bool? isWholeMatch = null;

            if (trEvent.SelectSingleNode("td[1]/a") != null) isWholeMatch = true;
            else
            {
                foreach (var partNameString in _matchPartKinds)
                {
                    if (trEvent.SelectSingleNode("td[1]").InnerText.Contains(partNameString))
                        isWholeMatch = false;
                }
            }
            if (isWholeMatch == null) return null;

            var tds = trEvent.SelectNodes("td");    //Столбцы с БК евентами
            if (tds == null) return null;
           
            var tmText = "";
            var tbText = "";
            var tmXpath = "";
            var tbXpath = "";
            var offset = 1;
            var partName = "";
            var partOfMatch = 0;
            //todo:  Придумать как быть с половинами

            if (isWholeMatch == true)
            {
                offset = 1;
                partOfMatch = 0;
                var playerInnerText = tds[1] == null ? "" : tds[1].InnerText;
                _player1 = Regex.Split(playerInnerText, " - ")[0].Trim().Replace("&nbsp;", "");
                _player2 = Regex.Split(playerInnerText, " - ")[1].Trim().Replace("&nbsp;", "");
                var nameTag = headerOfTable.SelectSingleNode("th[1]");
                _name = nameTag == null ? "" : nameTag.InnerText.Trim().Replace("&nbsp;", "");
            }
            else
            {
                offset = 0;
                var partTag = trEvent.SelectSingleNode("td[1]");
                partName = partTag == null ? "" : partTag.InnerText.Trim().Replace("&nbsp;", "");
                if (partName.Contains('1')) partOfMatch = 1;
                if (partName.Contains('2')) partOfMatch = 2;
                if (partName.Contains('3')) partOfMatch = 3;
                if (partName.Contains('4')) partOfMatch = 4;
                if (partName.Contains('5')) partOfMatch = 5;
            }


            if (!_columnNumbersByBkEvents.ContainsKey(BkEvent.Total) ||
                !_columnNumbersByBkEvents.ContainsKey(BkEvent.Tm) ||
                !_columnNumbersByBkEvents.ContainsKey(BkEvent.Tb))
                return null;

            var totalCell = tds[_columnNumbersByBkEvents[BkEvent.Total] + offset];
            var tmCell = tds[_columnNumbersByBkEvents[BkEvent.Tm] + offset];
            var tbCell = tds[_columnNumbersByBkEvents[BkEvent.Tb] + offset];
            var sportType = GetSportType(headerOfTable.InnerText.Trim());

            

            var cefs =  GetTotalCef(tmCell, tbCell, totalCell);
            
            var result = cefs.Select(cef => new SportEvent()
            {
                MainCef = cef.Item1, AdditionalCef = cef.Item2, SportType = sportType, Description = _name, Player1 = _player1, Player2 = _player2, PartOfMatchNumber = partOfMatch, PartOfMatchName = partName
            }).ToList();

            return result;
        }


        private void FillColumnNumbersByBkEvents(HtmlNode row)
        {
            _columnNumbersByBkEvents = new Dictionary<BkEvent, int>();
            var tds = row.SelectNodes("*");
            foreach (var td in tds)
            {
                var clearText = td.InnerText.Replace("&nbsp;", "");
                if (clearText == "П1") _columnNumbersByBkEvents[BkEvent.P1] = tds.IndexOf(td);
                else if (clearText == "П2") _columnNumbersByBkEvents[BkEvent.P2] = tds.IndexOf(td);
                else if (clearText == "TM")
                {
                    _columnNumbersByBkEvents[BkEvent.Total] = tds.IndexOf(td);
                    _columnNumbersByBkEvents[BkEvent.Tm] = tds.IndexOf(td)-1;
                    _columnNumbersByBkEvents[BkEvent.Tb] = tds.IndexOf(td) + 1;
                }
            }
        }

        
        private SportType GetSportType(string input)
        {
            var lowstring = input.ToLower();
            if (lowstring.Contains("футбол"))
                return SportType.Football;
            if (lowstring.Contains("баскетбол"))
                return SportType.Basketball;
            if (lowstring.Contains("теннис") && !lowstring.Contains("настольный"))
                return SportType.Tennis;
            if (lowstring.Contains("хоккей"))
                return SportType.IceHockey;
            if (lowstring.Contains("волейбол"))
                return SportType.Voleyball;
            if (lowstring.Contains("гандбол"))
                return SportType.Gandball;
            if (lowstring.Contains("настольный теннис"))
                return SportType.TableTennis;
            return SportType.Null;
        }


        public List<SportEvent> GetSportEvents(string page)
        {
            var result = new List<SportEvent>();

            var docTennisi = new HtmlDocument { OptionFixNestedTags = true };
            docTennisi.Load(new StringReader(page));

            var tablesTennisi = docTennisi.DocumentNode.SelectNodes("//*[@id='tb_content']/table[@style]");
            if (tablesTennisi == null) return result;

            foreach (var table in tablesTennisi)
            {
                var headerOfTable = table.SelectSingleNode("tr[1]"); //Заголовок таблицы
                

                FillColumnNumbersByBkEvents(headerOfTable);

                //Парсим розовые строчки
                var trsBkEvents = table.SelectNodes("tr");

                foreach (var trEvent in trsBkEvents)
                {
                    if (!trEvent.HasAttributes || trEvent.Attributes["bgcolor"].Value != "#FFDECF") continue;

                    var ev = ParsePinkRow(trEvent, headerOfTable);
                    if (ev == null || ev.Count == 0) continue;
                    result.AddRange(ev);
                   
                    //Парсим желтую табличку
                    if (trsBkEvents.IndexOf(trEvent) + 1 >= trsBkEvents.Count) continue;
                   
                    var nextRow = trsBkEvents[trsBkEvents.IndexOf(trEvent) + 1];
                    if (nextRow != null && nextRow.HasAttributes && nextRow.Attributes["bgcolor"].Value == "#FFF2C5")
                    {
                        var cefs = ParseYellowTables(nextRow);
                        result.AddRange(cefs.Select(cef => new SportEvent()
                        {
                            MainCef = cef.Item1, 
                            AdditionalCef = cef.Item2, 
                            SportType = ev[0].SportType, 
                            Description = ev[0].Description, 
                            Player1 = ev[0].Player1, 
                            Player2 = ev[0].Player2, 
                            PartOfMatchName = ev[0].PartOfMatchName, 
                            PartOfMatchNumber = ev[0].PartOfMatchNumber
                        }));
                    }
                }
            }
            return result;
        }




        private List<Tuple<Coefficient, Coefficient>> GetTotalCef(HtmlNode tmTag, HtmlNode tbTag, HtmlNode totalTag)
        {
            string tmText = "";
            string tbText = "";
            var totalText = totalTag == null ? "0" : totalTag.InnerText.Replace("&nbsp;", "").Replace("&lt;", "").Replace("&gt;", "").Trim();
            double tm, tb, total;

            string tmXpath = "", tbXpath = "";

            if (tmTag != null)
            {
                tmText = tmTag.InnerText.Trim().Replace("&nbsp;", "").Replace("&lt;", "").Replace("&gt;", "").Trim();
                var tmId = tmTag.SelectSingleNode("span") == null ? "" : tmTag.SelectSingleNode("span").Attributes["id"].Value;
                tmXpath = string.Format(@"//*[@id='{0}']", tmId);
            }

            if (tbTag != null)
            {
                tbText = tbTag.InnerText.Trim().Replace("&nbsp;", "").Replace("&lt;", "").Replace("&gt;", "").Trim();
                var tbId = tbTag.SelectSingleNode("span") == null ? "" : tbTag.SelectSingleNode("span").Attributes["id"].Value;
                tbXpath = string.Format(@"//*[@id='{0}']", tbId);
            }

            double.TryParse(tmText, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out tm);
            double.TryParse(tbText, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out tb);
            double.TryParse(totalText, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out total);
            
            var tmCef = new Coefficient(BkEvent.Tm, tm, tmXpath);
            var tbCef = new Coefficient(BkEvent.Tb, tb, tbXpath);
            var totalCef = new Coefficient(BkEvent.Total, total);
            return new List<Tuple<Coefficient, Coefficient>>()
            {
                new Tuple<Coefficient, Coefficient>(tmCef, totalCef),
                new Tuple<Coefficient, Coefficient>(tbCef, totalCef)
            };
        }


        private List<Tuple<Coefficient,Coefficient>> ParseYellowTables(HtmlNode trEvent)
        {
            var trs = trEvent.SelectNodes(".//tr");
            var result = new List<Tuple<Coefficient, Coefficient>>();
            foreach (var tr in trs)
            {
                var eventTag = tr.SelectSingleNode("td[1]");
                if (eventTag == null) continue;

                var tmTag = eventTag.SelectSingleNode("td[2]/span[@class='tt_l']");
                var tbTag = eventTag.SelectSingleNode("td[2]/span[@class='tt_r']");
                var totalTag = eventTag.SelectSingleNode("td[2]/span[@class='tt_c']");
                
                if (eventTag.InnerText.ToLower().Contains("тотал матча") ||
                    eventTag.InnerText.ToLower().Contains("тотал перво") ||
                    eventTag.InnerText.ToLower().Contains("тотал второ") ||
                    eventTag.InnerText.ToLower().Contains("тотал третье") ||
                    eventTag.InnerText.ToLower().Contains("тотал четверто") ||
                    eventTag.InnerText.ToLower().Contains("тотал пято")
                    )
                {
                    result.AddRange(GetTotalCef(tmTag, tbTag, totalTag));
                    
                }
            }
            return result;
        }

     
    }
}
