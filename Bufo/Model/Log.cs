﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bufo.Model
{
    public static class Log
    {
        public static string Filename { get; set; }

        static Log()
        {
            var filename = "bufo.log";
            Filename = Path.Combine(Environment.CurrentDirectory, filename);
        }

        public static void ToFile(string message)
        {
            if (!File.Exists(Filename))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(Filename))
                {
                    sw.WriteLine(message);
                }
            }

            // This text is always added, making the file longer over time
            // if it is not deleted.
            using (StreamWriter sw = File.AppendText(Filename))
            {
                sw.WriteLine(message);
            }
        }

    }
}
