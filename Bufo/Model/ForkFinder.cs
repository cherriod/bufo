﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Navigation;

namespace Bufo.Model
{
    public class ForkFinder
    {
        public SportEvent SportEvent1 { get; private set; }
        public SportEvent SportEvent2 { get; private set; }

        public List<Fork> Forks { get; private set; } 

        public double TotalAmount { get; set; }

        public int Round { get; set; }

        public ForkFinder(SportEvent sportEvent1, SportEvent sportEvent2, double totalAmount, int round)
        {
            SportEvent1 = sportEvent1;
            SportEvent2 = sportEvent2;
            TotalAmount = totalAmount < 0 ? 0 : totalAmount;
            Round = round;
            Forks = new List<Fork>();

            var fork = GetForkByTotal();
            if (fork != null)
                    Forks.Add(fork);


           
                fork = GetForkByWin();
                if (fork != null)
                    Forks.Add(fork);
            
        }

        private Fork GetForkByWin()
        {
            var canDraw = SportEvent1.CanDraw || SportEvent2.CanDraw;
                
            var drawExodus = (SportEvent1.MainCef.BkEventName == BkEvent.P1 &&
                              SportEvent2.MainCef.BkEventName == BkEvent.P2X) ||
                             (SportEvent1.MainCef.BkEventName == BkEvent.P2 &&
                              SportEvent2.MainCef.BkEventName == BkEvent.P1X) ||
                             (SportEvent1.MainCef.BkEventName == BkEvent.P12 &&
                              SportEvent2.MainCef.BkEventName == BkEvent.X) ||
                             (SportEvent1.MainCef.BkEventName == BkEvent.X &&
                              SportEvent2.MainCef.BkEventName == BkEvent.P12);

             var nonDrawExodus = (SportEvent1.MainCef.BkEventName == BkEvent.P1 &&
                                 SportEvent2.MainCef.BkEventName == BkEvent.P2) ||
                                (SportEvent1.MainCef.BkEventName == BkEvent.P2 &&
                                 SportEvent2.MainCef.BkEventName == BkEvent.P1);

            if ((canDraw && drawExodus) ||(!canDraw && nonDrawExodus)) 
            
            return (1 / SportEvent1.MainCef.Value + 1 / SportEvent2.MainCef.Value) > 1 ? null : new Fork(TotalAmount, SportEvent1.MainCef, SportEvent2.MainCef, Round);
            return null;
        }

        private Fork GetForkByTotal()
        {
            if (SportEvent1.AdditionalCef == null || SportEvent2.AdditionalCef == null) return null;
            if (SportEvent1.AdditionalCef.BkEventName != BkEvent.Total || SportEvent2.AdditionalCef.BkEventName != BkEvent.Total) return null;

            if ((1/SportEvent1.MainCef.Value + 1/SportEvent2.MainCef.Value) > 1) return null;
            
            if (SportEvent1.MainCef.BkEventName == BkEvent.Tm && SportEvent2.MainCef.BkEventName == BkEvent.Tb)
            {
                if (SportEvent1.AdditionalCef.Value >= SportEvent2.AdditionalCef.Value)
                    return new Fork(TotalAmount, SportEvent1.MainCef, SportEvent2.MainCef, SportEvent1.AdditionalCef, SportEvent2.AdditionalCef, Round);
            }

            if (SportEvent1.MainCef.BkEventName == BkEvent.Tb && SportEvent2.MainCef.BkEventName == BkEvent.Tm)
            {
                if (SportEvent1.AdditionalCef.Value <= SportEvent2.AdditionalCef.Value)
                    return new Fork(TotalAmount, SportEvent1.MainCef, SportEvent2.MainCef, SportEvent1.AdditionalCef, SportEvent2.AdditionalCef, Round);
            }
            return null;
        }

       
      

        public static bool IsEqualSportEvents(SportEvent sportEvent1, SportEvent sportEvent2)
        {
            if (sportEvent1.SportType != sportEvent2.SportType) return false;
            if (sportEvent1.PartOfMatchNumber != sportEvent2.PartOfMatchNumber) return false;
            
            var mainCef1 = sportEvent1.MainCef;
            var mainCef2 = sportEvent2.MainCef;
            var addCef1 = sportEvent1.AdditionalCef;
            var addCef2 = sportEvent2.AdditionalCef;

            //Расширенные коэффициенты существуют и их тип разный - выход
            //Основные коэффициенты существуют и их тип одинаковый - выход
            if (addCef1 != null && addCef2 != null && addCef1.BkEventName != addCef2.BkEventName) return false;
            if (mainCef1 != null && mainCef2 != null && mainCef1.BkEventName == mainCef2.BkEventName) return false;

            if (string.IsNullOrEmpty(sportEvent1.Player1) ||
                        string.IsNullOrEmpty(sportEvent1.Player2) ||
                        string.IsNullOrEmpty(sportEvent2.Player1) ||
                        string.IsNullOrEmpty(sportEvent2.Player2)
                        )
                return false;


            var sportEvent1WordsInPlayer1 = sportEvent1.PlayerCode1.Trim().Split().ToList(); 
            var sportEvent1WordsInPlayer2 = sportEvent1.PlayerCode2.Trim().Split().ToList(); 
            var sportEvent2WordsInPlayer1 = sportEvent2.PlayerCode1.Trim().Split().ToList(); 
            var sportEvent2WordsInPlayer2 = sportEvent2.PlayerCode2.Trim().Split().ToList(); 

            var sameWordsInPlayer1 = sportEvent1WordsInPlayer1.Count(x => sportEvent2WordsInPlayer1.Contains(x));
            var sameWordsInPlayer2 = sportEvent1WordsInPlayer2.Count(x => sportEvent2WordsInPlayer2.Contains(x));

            var isEqualEvents = sameWordsInPlayer1 >= (sportEvent1WordsInPlayer1.Count + sportEvent1WordsInPlayer2.Count) * 0.25
                && sameWordsInPlayer2 >= (sportEvent2WordsInPlayer1.Count + sportEvent2WordsInPlayer2.Count) * 0.25;

/*
            if (isEqualEvents)
            {
                Console.WriteLine(code1Player1 + " " + code2Player1 + " " + code1Player2 + " " + code2Player2);
            }
            if (code1Player1 == code2Player1 && code1Player2 == code2Player2)
            {
                Console.WriteLine(code1Player1 + " " + code2Player1 + " " + code1Player2 + " " + code2Player2 + " " + isEqualEvents);
            }*/
            
            return isEqualEvents;
        }
    }
}
