﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Newtonsoft.Json;

namespace Bufo.Model
{
    class BkMarathon
    {

        class JsonData
        {
            public string Sn;
            public string Mn;
            public string Ewc;
            public long Cid;
            public string Prt;
            public string Ewf;
            public string Epr;
            public Dictionary<string, string> Prices;
        }


        public async Task<List<SportEvent>> GetSportEvents(string page)
        {
            var result = new List<SportEvent>();

            var docMarathon = new HtmlDocument { OptionFixNestedTags = true };
            docMarathon.Load(new StringReader(page));

            var bufoWebClient = new BufoWebClient();
            var keysTags =docMarathon.DocumentNode.SelectNodes("//div[@class='available-events-container']//div[@data-available-event-treeid]");
            var keys = keysTags.Select(x => x.Attributes["data-available-event-treeid"].Value);
            var urls = new List<string>();
            urls.AddRange(keys.Select(x=>"https://www.marathonbet.com/su/live/"+x));
            var tasks = urls.Select(url => bufoWebClient.GetPageAsync(new Uri(url), Encoding.UTF8)).ToList();
            var pages = new List<string>();

            foreach (var task in tasks)
                pages.Add(await task);

            foreach (var p in pages)
                result.AddRange(ParsePage(p));

            return result;
        }

        

        //private List<SportEvent> ParsePage(string page, SportEvent headerEvent)
        private List<SportEvent> ParsePage(string page)
        {
            var result = new List<SportEvent>();

            var doc = new HtmlDocument { OptionFixNestedTags = true };
            doc.Load(new StringReader(page));

            //todo:  Придумать как быть с половинами

            var sportTypeStr = doc.DocumentNode.SelectSingleNode("//*[contains(@class, 'sport-category-label')]").InnerText.ToLower();
            var descriptionStr = doc.DocumentNode.SelectSingleNode("//*[contains(@class, 'category-label')]").InnerText.ToLower();

            //Марафон иногда переворачивает команды
            var player1 = "";
            var player2 = "";
            var playersTag = doc.DocumentNode.SelectNodes("//*[contains(@class, 'command')]");
            var firstPosition = playersTag[0];
            var secondPosition = playersTag[1];
            var numberFirstPosition = firstPosition.SelectSingleNode(".//*[contains(@class, 'member-number')]").InnerText;
            var playerFirstPosition = firstPosition.SelectSingleNode(".//*[contains(@class, 'live-today-member-name nowrap')]").InnerText;
            var playerSecondPosition = secondPosition.SelectSingleNode(".//*[contains(@class, 'live-today-member-name nowrap')]").InnerText;
            if (numberFirstPosition.Contains("1"))
            {
                player1 = playerFirstPosition;
                player2 = playerSecondPosition;
            }
            else
            {
                player2 = playerFirstPosition;
                player1 = playerSecondPosition;
            }

            var tdsValues = doc.DocumentNode.SelectNodes("//td[contains(@class, 'height-column-with-price')]")
                .Where(x => x.HasAttributes && x.Attributes.Contains("data-sel"));
            var attributes = tdsValues.Select(x => x.Attributes["data-sel"].Value);
            var jsons = attributes.Select(x=> JsonConvert.DeserializeObject<JsonData>(x));
            foreach (var json in jsons)
            {
                var part = new Tuple<string, int>("", 0);
                var sportType = SportType.Null;
                
                switch (sportTypeStr)
                {
                    case "баскетбол":
                        sportType = SportType.Basketball;
                        part = GetPartOfBasketball(json);
                        break;
                    case "футбол":
                       sportType = SportType.Football;
                        part = GetPartOfFootball(json);
                        break;
                    case "гандбол":
                        sportType = SportType.Gandball;
                        part = GetPartOfGandball(json);
                        break;
                    case "хоккей":
                        sportType = SportType.IceHockey;
                        part = GetPartOfHockey(json);
                        break;
                    case "настольный теннис":
                        sportType = SportType.TableTennis;
                        part = GetPartOfTableTennis(json);
                        break;
                    case "теннис":
                        sportType = SportType.Tennis;
                        part = GetPartOfTennis(json);
                        break;
                    case "волейбол":
                        sportType = SportType.Voleyball;
                        part = GetPartOfVoleyball(json);
                        break;
                }
                if (sportType == SportType.Null || part == null) continue;

                Coefficient mainCef = GetWinCef(json, player1, player2);
                Coefficient addCef = null;

                if (mainCef == null)
                {
                    var totalCef = GetTotalCef(json);
                    mainCef = totalCef == null? null: totalCef.Item1;
                    addCef = totalCef == null ? null : totalCef.Item2;    
                }

                if (mainCef == null) continue;

                var sportEvent = new SportEvent()
                {
                    SportType = sportType,
                    Description = descriptionStr,
                    BkName = "Marathon",
                    Player1 = player1,
                    Player2 = player2,
                    PartOfMatchName = part.Item1,
                    PartOfMatchNumber = part.Item2,
                    MainCef = mainCef,
                    AdditionalCef = addCef
                };

                result.Add(sportEvent);
            }
            return result;
        }

    private Tuple<string, int> GetPartOfBasketball(JsonData json)
        {
            var cellPartStr = json.Mn.ToLower();

            switch (cellPartStr)
            {
                case "победитель матча, включая все от":
                    return new Tuple<string, int>("", 0);
                case "результат, 1-я четверть":
                    return new Tuple<string, int>("1-я четверть", 1);
                case "результат, 2-я четверть":
                    return new Tuple<string, int>("2-я четверть", 2);
                case "результат, 3-я четверть":
                    return new Tuple<string, int>("3-я четверть", 3);
                case "результат, 4-я четверть":
                    return new Tuple<string, int>("4-я четверть", 4);
                case "тотал очков":
                    return new Tuple<string, int>("", 0);
                case "тотал очков, 1-я четверть":
                    return new Tuple<string, int>("1-я четверть", 1);
                case "тотал очков, 2-я четверть":
                    return new Tuple<string, int>("2-я четверть", 2);
                case "тотал очков, 3-я четверть":
                    return new Tuple<string, int>("3-я четверть", 3);
                case "тотал очков, 4-я четверть":
                    return new Tuple<string, int>("4-я четверть", 4);
            }

        return null;
        }

    private Tuple<string, int> GetPartOfFootball(JsonData json)
        {
            var cellPartStr = json.Mn.ToLower();
            switch (cellPartStr)
            {
                case "результат матча":
                    return new Tuple<string, int>("", 0);
                case "результат, 1-й тайм":
                    return new Tuple<string, int>("1-й тайм", 1);
                case "результат, 2-й тайм":
                    return new Tuple<string, int>("2-й тайм", 2);
                case "тотал голов":
                    return new Tuple<string, int>("", 0);
                case "тотал голов, 1-й тайм":
                    return new Tuple<string, int>("1-й тайм", 1);
                case "тотал голов, 2-й тайм":
                    return new Tuple<string, int>("2-й тайм", 2);
            }
        return null;
        }

    private Tuple<string, int> GetPartOfGandball(JsonData json)
        {
            var cellPartStr = json.Mn.ToLower();
            var result = new Tuple<string, int>("", 0);
            switch (cellPartStr)
            {
                case "результат матча":
                    return new Tuple<string, int>("", 0);
                case "результат, 1-й тайм":
                    return new Tuple<string, int>("1-й тайм", 1);
                case "результат, 2-й тайм":
                    return new Tuple<string, int>("2-й тайм", 2);
                case "тотал голов":
                    return new Tuple<string, int>("", 0);
                case "тотал голов, 1-й тайм":
                    return new Tuple<string, int>("1-й тайм", 1);
                case "тотал голов, 2-й тайм":
                    return new Tuple<string, int>("2-й тайм", 2);
            }
            return null; //todo Переделать гандбол
        }

    private Tuple<string, int> GetPartOfHockey(JsonData json)
        {
            var cellPartStr = json.Mn.ToLower();
            
            switch (cellPartStr)
            {
                case "результат":
                    return new Tuple<string, int>("", 0);
                case "результат, 1-й период":
                    return new Tuple<string, int>("1-й период", 1);
                case "результат, 2-й период":
                    return new Tuple<string, int>("2-й период", 2);
                case "результат, 3-й период":
                    return new Tuple<string, int>("3-й период", 3);
                case "тотал голов":
                    return new Tuple<string, int>("", 0);
                case "тотал голов, 1-й период":
                    return new Tuple<string, int>("1-й период", 1);
                case "тотал голов, 2-й период":
                    return new Tuple<string, int>("2-й период", 2);
                case "тотал голов, 3-й период":
                    return new Tuple<string, int>("3-й период", 2);
            }
            return null;
        }

    private Tuple<string, int> GetPartOfTableTennis(JsonData json)
        {
            var cellPartStr = json.Mn.ToLower();
           
            switch (cellPartStr)
            {
                case "победа в матче":
                    return new Tuple<string, int>("", 0);
                case "результат, 1-я партия":
                    return new Tuple<string, int>("1-я партия", 1);
                case "результат, 2-я партия":
                    return new Tuple<string, int>("2-я партия", 2);
                case "результат, 3-я партия":
                    return new Tuple<string, int>("3-я партия", 3);
                case "результат, 4-я партия":
                    return new Tuple<string, int>("4-я партия", 4);
                case "результат, 5-я партия":
                    return new Tuple<string, int>("5-я партия", 5);
                case "тотал матча по очкам":
                    return  new Tuple<string, int>("", 0);
                case "тотал 1-й партии по очкам":
                    return new Tuple<string, int>("1-я партия", 1);
                case "тотал 2-й партии по очкам":
                    return new Tuple<string, int>("2-я партия", 2);
                case "тотал 3-й партии по очкам":
                    return new Tuple<string, int>("3-я партия", 3);
                case "тотал 4-й партии по очкам":
                    return new Tuple<string, int>("4-я партия", 4);
                case "тотал 5-й партии по очкам":
                    return new Tuple<string, int>("5-я партия", 5);
            }
        return null;
            
        }
    private Tuple<string, int> GetPartOfTennis(JsonData json)
        {
            var cellPartStr = json.Mn.ToLower();
           
            switch (cellPartStr)
            {
                case "результат матча":
                    return new Tuple<string, int>("", 0);
                case "результат, 1-й сет":
                    return new Tuple<string, int>("1-й сет", 1);
                case "результат, 2-й сет":
                    return new Tuple<string, int>("2-й сет", 2);
                case "результат, 3-й сет":
                    return new Tuple<string, int>("3-й сет", 3);
                case "результат, 4-й сет":
                    return new Tuple<string, int>("4-й сет", 4);
                case "результат, 5-й сет":
                    return new Tuple<string, int>("5-й сет", 5);
                case "тотал геймов":
                    return new Tuple<string, int>("", 0);
                case "тотал геймов, 1-й сет":
                    return new Tuple<string, int>("1-й сет", 1);
                case "тотал геймов, 2-й сет":
                    return new Tuple<string, int>("2-й сет", 2);
                case "тотал геймов, 3-й сет":
                    return new Tuple<string, int>("3-й сет", 3);
                case "тотал геймов, 4-й сет":
                    return new Tuple<string, int>("4-й сет", 4);
                case "тотал геймов, 5-й сет":
                    return new Tuple<string, int>("5-й сет", 5);
            }
            return null;
        }

    private Tuple<string, int> GetPartOfVoleyball(JsonData json)
        {
            var cellPartStr = json.Mn.ToLower();
           
            switch (cellPartStr)
            {
                case "победа в матче":
                    return new Tuple<string, int>("", 0);
                case "результат, 1-я партия":
                    return new Tuple<string, int>("1-я партия", 1);
                case "результат, 2-я партия":
                    return new Tuple<string, int>("2-я партия", 2);
                case "результат, 3-я партия":
                    return new Tuple<string, int>("3-я партия", 3);
                case "результат, 4-я партия":
                    return new Tuple<string, int>("4-я партия", 4);
                case "результат, 5-я партия":
                    return new Tuple<string, int>("5-я партия", 5);
                case "тотал матча по очкам":
                    return new Tuple<string, int>("", 0);
                case "тотал 1-й партии по очкам":
                    return new Tuple<string, int>("1-я партия", 1);
                case "тотал 2-й партии по очкам":
                    return new Tuple<string, int>("2-я партия", 2);
                case "тотал 3-й партии по очкам":
                    return new Tuple<string, int>("3-я партия", 3);
                case "тотал 4-й партии по очкам":
                    return new Tuple<string, int>("4-я партия", 4);
                case "тотал 5-й партии по очкам":
                    return new Tuple<string, int>("5-я партия", 5);
            }
            return null;
        }

     private Coefficient GetWinCef(JsonData json, string player1, string player2)
        {
            var winString = json.Sn.ToLower();
            var bkEventName = BkEvent.Null;
            if (winString == player1.ToLower()) bkEventName = BkEvent.P1;
            if (winString == player2.ToLower()) bkEventName = BkEvent.P2;
            if (winString == player1.ToLower() + " (победа)") bkEventName = BkEvent.P1;
            if (winString == player2.ToLower() + " (победа)") bkEventName = BkEvent.P2;
            if (winString == "ничья") bkEventName = BkEvent.X;
            if (winString == player1.ToLower() + " (победа) или ничья") bkEventName = BkEvent.P1X;
            if (winString == player2.ToLower() + " (победа) или ничья") bkEventName = BkEvent.P2X;
            if (winString == player1.ToLower() + " (победа) или " + player2.ToLower() + " (победа)") bkEventName = BkEvent.P12;

            if (bkEventName == BkEvent.Null) return null;
            var priceStr = json.Epr;
            double price;
            double.TryParse(priceStr, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out price);

            return new Coefficient(bkEventName, price);
        }

        private Tuple<Coefficient, Coefficient> GetTotalCef(JsonData json)
        {
            var totalStrArray = json.Sn.Split();
            if (totalStrArray.Length < 2) return null;

            var totalStr = totalStrArray[1];
            double total;
            double.TryParse(totalStr, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out total);
            Coefficient additionalCef = new Coefficient(BkEvent.Total, total);

            var priceStr = json.Epr;
            double price;
            double.TryParse(priceStr, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out price);

            Coefficient mainCef = null;
            if (totalStrArray[0].ToLower().Contains("меньше"))
                mainCef = new Coefficient(BkEvent.Tm, price);
            else if (totalStrArray[0].ToLower().Contains("больше"))
                mainCef = new Coefficient(BkEvent.Tb, price);
            if (mainCef == null) return null;

           
            return new Tuple<Coefficient, Coefficient>(mainCef, additionalCef);
        }
    }
}
