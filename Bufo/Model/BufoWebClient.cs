﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bufo.Model
{
    class BufoWebClient
    {

        public static bool UseProxy { get; set; }

        public static string ProxyAddress { get; set; }

        public static int ProxyPort { get; set; }

        public static void SetProxy(string address, int port)
        {
            ProxyAddress = address;
            ProxyPort = port;
            UseProxy = true;
        }

        public static void UnsetProxy()
        {
            UseProxy = false;
        }


        public async Task<string> GetPageAsync(Uri uri, Encoding encoding=null)
        {
            
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 |
                                                              SecurityProtocolType.Tls;

            ServicePointManager.ServerCertificateValidationCallback = (sen, certificate, chain, sslPolicyErrors) => { return true; };
            //SSLValidator.OverrideValidation();  
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            //request.Method = WebRequestMethods.Http.Get;
            //request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0";
            //request.ContentType = "application/json; charset=utf-8";
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            //WebHeaderCollection myWebHeaderCollection = request.Headers;
            //myWebHeaderCollection.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
            //myWebHeaderCollection.Add("Accept-Encoding", "gzip, deflate, br");

            if (UseProxy && !string.IsNullOrEmpty(ProxyAddress) && (ProxyPort > 0) && (ProxyPort < 65536))
            {
                var myproxy = new WebProxy(ProxyAddress, ProxyPort);
                myproxy.BypassProxyOnLocal = false;
                request.Proxy = myproxy;
            }
            else request.Proxy = null;
            request.Method = "GET";

            try
            {
                var response = await request.GetResponseAsync();
                var data = response.GetResponseStream();
                //StreamReader reader = new StreamReader(data, encoding ?? Encoding.Default);
                //var page = await reader.ReadToEndAsync();
                string page;

                using (var reader = new StreamReader(data, encoding ?? Encoding.Default))
                {
                    //This allows you to do one Read operation.
                    page = await reader.ReadToEndAsync();
                }

                return page;
            }
            catch (Exception ex)
            {

                throw;
            }

            
        }

        public string GetPage(Uri uri, Encoding encoding = null)
        {
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 |
                                                              SecurityProtocolType.Tls;

            ServicePointManager.ServerCertificateValidationCallback = (sen, certificate, chain, sslPolicyErrors) => { return true; };
            //SSLValidator.OverrideValidation();  
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            //request.Method = WebRequestMethods.Http.Get;
            //request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0";
            //request.ContentType = "application/json; charset=utf-8";
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            //WebHeaderCollection myWebHeaderCollection = request.Headers;
            //myWebHeaderCollection.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3");
            //myWebHeaderCollection.Add("Accept-Encoding", "gzip, deflate, br");

            if (UseProxy && !string.IsNullOrEmpty(ProxyAddress) && (ProxyPort > 0) && (ProxyPort < 65536))
            {
                var myproxy = new WebProxy(ProxyAddress, ProxyPort);
                myproxy.BypassProxyOnLocal = false;
                request.Proxy = myproxy;
            }
            else request.Proxy = null;
            request.Method = "GET";

            try
            {
                var response = request.GetResponse();
                var data = response.GetResponseStream();
                //StreamReader reader = new StreamReader(data, encoding ?? Encoding.Default);
                //var page = await reader.ReadToEndAsync();
                string page;

                using (var reader = new StreamReader(data, encoding ?? Encoding.Default))
                {
                    //This allows you to do one Read operation.
                    page = reader.ReadToEnd();
                }

                return page;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

    }
}
