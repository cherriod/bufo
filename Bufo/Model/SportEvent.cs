﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Bufo.Model
{
    
    public enum PartOfMatch
    {
        Match, //Целый матч
        Period1, //1-й период
        Period2, //2-й период
        Period3, //3-й период
        Time1, //1-й тайм
        Time2, //2-й тайм
        Set1, //1-й сет
        Set2, //2-й сет
        Set3, //3-й сет
        Set4, //3-й сет
        Half1, //1-я половина
        Half2, //2-я половина
        Quater1, //1-я четверть
        Quater2, //2-я четверть
        Quater3, //3-я четверть
        Quater4, //4-я четверть
    }

    public enum SportType
    {
        Football,
        Basketball,
        Tennis,
        IceHockey,
        Voleyball,
        Gandball,
        TableTennis,
        Null
    }


    public class SportEvent
    {
        private string _player1;
        private string _player2;

        public string BkName { get; set; }
        public string Description { get; set; }

        public string PlayerCode1 { get; private set; }
        public string PlayerCode2 { get; private set; }

        public string Player1
        {
            get
            {
                return _player1; 
                
            }
            set
            {
                _player1 = value;
                PlayerCode1 = Metaphone(value);
            }
        }

        public string Player2
        {
            get { return _player2; }
            set
            {
                _player2 = value;
                PlayerCode2 = Metaphone(value);
            }
        }


        public Coefficient MainCef { get; set; }
        public Coefficient AdditionalCef { get; set; }
        public int PartOfMatchNumber { get; set; }
        public string PartOfMatchName { get; set; }
        public PartOfMatch PartOfMatch { get; set; }
        public SportType SportType { get; set; }
        public bool CanDraw {
            get
            {
                // По умолчанию CanDraw = True безопаснее, потому что тогда сравнение будет одноисходного и двуисходного коэффициентов
                // и если ничьей не может быть, то двуисходный будет отсутствовать, сравнение не пройдет.
                if (PartOfMatchNumber == 0)
                {
                    if (SportType == SportType.TableTennis || SportType == SportType.Tennis ||
                        SportType == SportType.Basketball || SportType == SportType.Voleyball)
                        return false;
                }
                else
                {
                    if (SportType == SportType.Tennis || SportType == SportType.Voleyball ||
                        SportType == SportType.TableTennis)
                        return false;
                }
                return true;
            } 
        }

        public SportEvent()
        {
            PartOfMatch = PartOfMatch.Match;
            SportType = SportType.Null;
        }

      

        public string Metaphone(string input)
        {
            var result = "";
            var separators = new[] { ' ', '-' };

            var r = new Regex(@"([A-Za-z0-9-\.,()]+)");
            
            char[] consonants = { 'б', 'в', 'г', 'д', 'ж', 'з', 'й', 'к', 'л', 'м', 'н', 'п', 'р', 'с', 'т', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ь' };
            char[] consonantsAdditional = { 'б', 'в', 'г', 'д', 'ж', 'з', 'й', 'к', 'п', 'с', 'т', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ь' };

            var words = input.ToLower().Split(separators).ToList();

            foreach (var word in words)
            {
                var filterstring = new StringBuilder(r.Replace(word, ""));
                if (filterstring.ToString() == string.Empty) continue;

                filterstring.Replace("йо", "и").Replace("ио", "и").Replace("йе", "и").Replace("ие", "и");
                filterstring.Replace("о", "а").Replace("ы", "а").Replace("я", "а");
                filterstring.Replace("е", "и").Replace("ё", "и").Replace("э", "и");
                filterstring.Replace("ю", "у");

                for (var i = 0; i < filterstring.Length; i++)
                {
                    if (i == filterstring.Length - 1 && consonants.Contains(filterstring[i]) ||
                        i < filterstring.Length - 1 && consonants.Contains(filterstring[i]) &&
                        consonantsAdditional.Contains(filterstring[i + 1]))
                    {
                        if (filterstring[i] == 'б') filterstring[i] = 'п';
                        if (filterstring[i] == 'з') filterstring[i] = 'с';
                        if (filterstring[i] == 'д') filterstring[i] = 'т';
                        if (filterstring[i] == 'в') filterstring[i] = 'ф';
                        if (filterstring[i] == 'г') filterstring[i] = 'к';
                    }
                }
                filterstring.Replace("тс", "ц");
                filterstring.Replace("дс", "ц");

                result += filterstring + " ";
            }
            return result;
        }
    }
}
