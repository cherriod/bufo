﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using HtmlAgilityPack;
using Newtonsoft.Json;

namespace Bufo.Model
{
    class BkFonbet
    {
        class JsonData
        {
            public string PacketVersion;
            public string FromVersion;
            public string FactorsVersion;
            public string SiteVersion;
            public List<Sport> Sports;
            public List<Events> Events;
            public List<CustomFactors> CustomFactors;
        }

        class Sport
        {
            public int Id;
            public int ParentId;
            public string Kind;
            public string SortOrder;
            public string Name;
        }

        class Events
        {
            public int Id;
            public string SortOrder;
            public int Level;
            public int Num;
            public int SportId;
            public int Kind;
            public int RootKind;
            public string Team1;
            public string Team2;
            public string Name;
            public string NamePrefix;
            public int StartTime;
            public string Place;
            public int Priority;
            public int ParentId;
        }

        class CustomFactors
        {
            public int E;       // Sport Event ID
            public string F;    // BK Event ID
            public double V;    // BK Event Value
            public int P;       // Bet Value * 100
            public string Pt;   // Bet Value in string
            public bool IsLive;
        }

        readonly List<int> _matchPartKinds = new List<int>()
                {
                    100101, //1-й период
                    100102, //2-й период
                    100103, //3-й период
                    100201, //1-й тайм
                    100202, //2-й тайм
                    100501, //1-й сет
                    100502, //2-й сет
                    100503, //3-й сет
                    100301, //1-я половина
                    100302, //2-я половина
                    100401, //1-я четверть
                    100402, //2-я четверть
                    100403, //3-я четверть
                    100404, //4-я четверть
                };

        private SportType GetSportTypeByTag(Sport sportTag)
        {
            if (sportTag.Id == 1 || sportTag.ParentId == 1)
                return SportType.Football;
            if (sportTag.Id == 2 || sportTag.ParentId == 2)
                return SportType.IceHockey;
            if (sportTag.Id == 2 || sportTag.ParentId == 2)
                return SportType.Basketball;
            if (sportTag.Id == 9 || sportTag.ParentId == 9)
                return SportType.Voleyball;
            if (sportTag.Id == 4 || sportTag.ParentId == 4)
                return SportType.Tennis;
            if (sportTag.Id == 8 || sportTag.ParentId == 8)
                return SportType.Gandball;
            if (sportTag.Id == 3088 || sportTag.ParentId == 8)
                return SportType.TableTennis;
            return SportType.Null;
        }

    
        public List<SportEvent> GetSportEvents(string page)
        {
            var result = new List<SportEvent>();
            //var page1 =  File.ReadAllText("fonbet.json");
            var jsonData = JsonConvert.DeserializeObject<JsonData>(page);


            var events = jsonData.Events.Where(x => x.ParentId == 0 || _matchPartKinds.Contains(x.Kind));
            foreach (var jsonEvent in events)
            {
                
                
                var sportTypeTag = jsonData.Sports.FirstOrDefault(x => x.Id == jsonEvent.SportId);
                if (sportTypeTag == null) continue;
                var sportType = GetSportTypeByTag(sportTypeTag);
                //var sportType = SportType.Null;
                //if (sportTypeTag.Id == 1 || sportTypeTag.ParentId == 1) sportType = SportType.Football;
                var playersTag = jsonData.Events.FirstOrDefault(x => x.Id == jsonEvent.ParentId);

                var totalCefs = GetTotalCefs(jsonData, jsonEvent);
                foreach (var totalCef in totalCefs)
                {
                    result.Add(new SportEvent()
                    {
                        BkName = "Fonbet",
                        SportType = sportType,
                        Description = sportTypeTag.Name,
                        PartOfMatchName = jsonEvent.Name,
                        Player1 = playersTag != null ? playersTag.Team1 : jsonEvent.Team1,
                        Player2 = playersTag != null ? playersTag.Team2 : jsonEvent.Team2,
                        AdditionalCef = totalCef[BkEvent.Total],
                        MainCef = totalCef[BkEvent.Tm],
                        PartOfMatchNumber = jsonEvent.Kind.ToString().Length == 6 ? jsonEvent.Kind%10 : 0
                    });

                    result.Add(new SportEvent()
                    {
                        BkName = "Fonbet",
                        SportType = sportType,
                        Description = sportTypeTag.Name,
                        PartOfMatchName = jsonEvent.Name,
                        Player1 = playersTag != null ? playersTag.Team1 : jsonEvent.Team1,
                        Player2 = playersTag != null ? playersTag.Team2 : jsonEvent.Team2,
                        AdditionalCef = totalCef[BkEvent.Total],
                        MainCef = totalCef[BkEvent.Tb],
                        PartOfMatchNumber = jsonEvent.Kind.ToString().Length == 6 ? jsonEvent.Kind%10 : 0
                    });
                }

                var winCefs = GetWinCefs(jsonData, jsonEvent);
                foreach (var wincef in winCefs)
                {
                    result.Add(new SportEvent()
                    {
                        BkName = "Fonbet",
                        SportType = sportType,
                        Description = sportTypeTag.Name,
                        PartOfMatchName = jsonEvent.Name,
                        Player1 = playersTag != null ? playersTag.Team1 : jsonEvent.Team1,
                        Player2 = playersTag != null ? playersTag.Team2 : jsonEvent.Team2,
                        MainCef = wincef,
                        PartOfMatchNumber = jsonEvent.Kind.ToString().Length == 6 ? jsonEvent.Kind % 10 : 0
                    });
                }
            }
            return result;
        }

      
        private readonly List<Dictionary<BkEvent, string>> _totalCefList = new List<Dictionary<BkEvent, string>>()
        {
               new Dictionary<BkEvent, string>()
                {
                    {BkEvent.Tm, "931"},
                    {BkEvent.Tb, "930"}
                },
                 new Dictionary<BkEvent, string>()
                {
                    {BkEvent.Tm, "941"},
                    {BkEvent.Tb, "940"}
                },
                 new Dictionary<BkEvent, string>()
                {
                    {BkEvent.Tm, "1697"},
                    {BkEvent.Tb, "1696"}
                },
                 new Dictionary<BkEvent, string>()
                {
                    {BkEvent.Tm, "1728"},
                    {BkEvent.Tb, "1727"}
                },
                 new Dictionary<BkEvent, string>()
                {
                    {BkEvent.Tm, "1731"},
                    {BkEvent.Tb, "1730"}
                },
                 new Dictionary<BkEvent, string>()
                {
                    {BkEvent.Tm, "1849"},
                    {BkEvent.Tb, "1848"}
                }
        };


        private List<Coefficient> GetWinCefs(JsonData jsonData, Events jsonEvent)
        {
            var result = new List<Coefficient>();
            var _p1 = jsonData.CustomFactors.Where(x => x.E == jsonEvent.Id).FirstOrDefault(x => (x.F == "921"));
            var _x = jsonData.CustomFactors.Where(x => x.E == jsonEvent.Id).FirstOrDefault(x => (x.F == "922"));
            var _p2 = jsonData.CustomFactors.Where(x => x.E == jsonEvent.Id).FirstOrDefault(x => (x.F == "923"));
            var _1x = jsonData.CustomFactors.Where(x => x.E == jsonEvent.Id).FirstOrDefault(x => (x.F == "924"));
            var _12 = jsonData.CustomFactors.Where(x => x.E == jsonEvent.Id).FirstOrDefault(x => (x.F == "1571"));
            var _x2 = jsonData.CustomFactors.Where(x => x.E == jsonEvent.Id).FirstOrDefault(x => (x.F == "925"));

            if (_p1 != null) result.Add(new Coefficient(BkEvent.P1, _p1.V));
            if (_x != null) result.Add(new Coefficient(BkEvent.X, _x.V));
            if (_p2 != null) result.Add(new Coefficient(BkEvent.P2, _p2.V));
            if (_1x != null) result.Add(new Coefficient(BkEvent.P1X, _1x.V));
            if (_12 != null) result.Add(new Coefficient(BkEvent.P12, _12.V));
            if (_x2 != null) result.Add(new Coefficient(BkEvent.P2X, _x2.V));


            //todo:Вставить XPATH

            return result;
        }

        private List<Dictionary<BkEvent, Coefficient>> GetTotalCefs(JsonData jsonData, Events jsonEvent)
        {
            var result = new List<Dictionary<BkEvent, Coefficient>>() ;

            //1849 931 941 1697 1728 1731 TotalM
            //var totalM =
            //    jsonData.CustomFactors.Where(x => x.E == jsonEvent.Id)
            //        .FirstOrDefault(x => (x.F == "931") || (x.F == "941") || (x.F == "1849") || (x.F == "1697") || (x.F == "1728") || (x.F == "1731"));

            //1848 930 940 1696 1727 1730 - TotalB
            //var totalB =
            //    jsonData.CustomFactors.Where(x => x.E == jsonEvent.Id)
            //        .FirstOrDefault(x => x.F == "930" || (x.F == "940") || (x.F == "1848") || (x.F == "1696") || (x.F == "1727") || (x.F == "1730"));

            foreach (var totalCefs in _totalCefList)
            {
                var totalM = jsonData.CustomFactors.Where(x => x.E == jsonEvent.Id)
                    .FirstOrDefault(x => (x.F == totalCefs[BkEvent.Tm]));
                var totalB = jsonData.CustomFactors.Where(x => x.E == jsonEvent.Id)
                    .FirstOrDefault(x => (x.F == totalCefs[BkEvent.Tb]));
                if (totalM == null || totalB == null) continue;
                
                var tmXpath = string.Format(@"//*[@id='event{0}under']", jsonEvent.Id); //*[@id="event7002450under"]
                var tbXpath = string.Format(@"//*[@id='event{0}over']", jsonEvent.Id); //*[@id="event7002450over"]
                
                var tm = new Coefficient(BkEvent.Tm, totalM.V, tmXpath);
                var tb = new Coefficient(BkEvent.Tb, totalB.V, tbXpath);
                var total = new Coefficient(BkEvent.Total, (double)totalB.P / 100);

                result.Add(new Dictionary<BkEvent, Coefficient>()
                {
                    {BkEvent.Total, total},
                    {BkEvent.Tm, tm},
                    {BkEvent.Tb, tb}
                });

            }
            return result;
        }
    }
}
