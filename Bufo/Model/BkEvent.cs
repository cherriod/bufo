﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bufo.Model
{
    public enum BkEvent
    {
        Total,
        Tm,
        Tb,
        P1,
        P2,
        X,
        P1X,
        P2X,
        P12,
        Null
    }

    public class Coefficient: IComparable<Coefficient>
    {
        public BkEvent BkEventName { get; private set; }
        public double Value { get; private set; }
        public string Xpath { get; private set; }


        public Coefficient(BkEvent bkEventName, double value)
        {
            BkEventName = bkEventName;
            Value = value;
        }
        public Coefficient(BkEvent bkEventName, double value, string xpath):this(bkEventName, value)
        {
            Xpath = xpath;
        }

    



        public int CompareTo(Coefficient other)
        {
            return Value.CompareTo(other.Value) != 0 ? Value.CompareTo(other.Value) : 0;
        }

        public override string ToString()
        {
            return Value.ToString();
        }
    }

}
