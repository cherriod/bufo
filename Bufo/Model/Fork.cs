﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bufo.Model
{
    public class Fork
    {

        /// <summary>
        /// Ставка на первого игрока
        /// </summary>
        public double AmountPlayer1 { get; private set; }

        /// <summary>
        /// Ставка на второго игрока
        /// </summary>
        public double AmountPlayer2 { get; private set; }

        public Coefficient Coefficient1 { get; private set; }
        public Coefficient Coefficient2 { get; private set; }
        public Coefficient AdditionalCef1 { get; private set; }
        public Coefficient AdditionalCef2 { get; private set; }

        /// <summary>
        /// Выхлоп
        /// </summary>
        public double ProfitPercent { get; private set; }
        public double Profit1 { get; private set; }
        public double Profit2 { get; private set; }
       
        public int Round { get; private set; }
    

        public Fork(double totalAmount, Coefficient coefficient1, Coefficient coefficient2, Coefficient additionalCef1=null, Coefficient additionalCef2=null,
            int round=0)
        {
            Round = round;
            Coefficient1 = coefficient1;
            Coefficient2 = coefficient2;
            AdditionalCef1 = additionalCef1;
            AdditionalCef2 = additionalCef2;
            AmountPlayer1 = Coefficient2.Value == Coefficient1.Value ? MyRound(totalAmount / 2, round) : MyRound(totalAmount * Coefficient2.Value / (Coefficient2.Value + Coefficient1.Value), round);
            AmountPlayer2 = MyRound(totalAmount - AmountPlayer1, round);
            ProfitPercent = Math.Round((1 - (1 / Coefficient1.Value + 1 / Coefficient2.Value)) * 100, 2);
            Profit1 = Math.Round(AmountPlayer1 * Coefficient1.Value - AmountPlayer1 - AmountPlayer2, 2);
            Profit2 = Math.Round(AmountPlayer2 * Coefficient2.Value - AmountPlayer1 - AmountPlayer2, 2);
        }

        public Fork(double totalAmount, Coefficient coefficient1, Coefficient coefficient2, int round = 0)
            :this(totalAmount, coefficient1, coefficient2, null, null, round)
        {
        }



        private double MyRound(double value, int sign)
        {

            if (sign < 0)
                return Math.Round(value/10/Math.Abs(sign))*10*Math.Abs(sign);
            if(sign > 2)
                return Math.Round(value, 2);
            return Math.Round(value, sign);
        }

        public static bool IsFork(Coefficient coefficient1, Coefficient coefficient2)
        {
            return (1/coefficient1.Value + 1/coefficient2.Value) < 1;
        }


    /*    public static bool IsFork(Coefficient coefficient1, Coefficient coefficient2, Coefficient additionalCef1,
            Coefficient additionalCef2)
        {
            if ((1 / coefficient1.Value + 1 / coefficient2.Value) < 1 && (additionalCef2.Value >= additionalCef1.Value))
                return true;
            if ((1 / coefficient1.Value + 1 / coefficient2.Value) < 1 && (additionalCef1.Value >= additionalCef2.Value))
                return true;
            return false;
        }*/
       
    }
}
