﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Text;
using System.Xml.Schema;
using Bufo.Model;
using Newtonsoft.Json;

namespace Bufo
{
   

    class BkBetcity
    {
        class JsonData
        {
            public int Lng;
            public bool Ok;
            public string FactorsVersion;
            public string SiteVersion;
            public Reply Reply;
        }

        class Reply
        {
            public string Curr_time;
            public string Ntime;
            public string Ntime_templates;
            public Dictionary<int, Sport> Sports  { get; set; }
        }

        class Sport
        {
            public Dictionary<int, Chmp> Chmps { get; set; }
            public int Id_sp;
            public string Name_sp; //Футбол
            public int Order;
        }

        class Chmp
        {
            public Dictionary<int, Evt> Evts { get; set; }
            public int Id_ch;
            public string Name_ch; //Футбол Чемпионат японии Мужчины
            public string Order;
        }

        class Evt
        {
            public string bet_num;
            public string cnt_ext_add;
            public string curr_gives;
            public string date_ev;
            public string date_ev_str;
            public string del;
            public string del_ev;
            public Dictionary<int, Ev> Ext { get; set; }    //Скрытые евенты
            public string game_type;
            public string id_at;
            public string id_ev;
            public string id_ht;
            public string is_in_line;
            public string is_online;
            public string iso_f;
            public string iso_s;
            public Dictionary<int, Ev> Main { get; set; }   //Евенты в строке
            public string maximum;
            public string md;
            public string md_ev;
            public string name_at; //=Тандерс
            public string name_ht; //=Сантори
            public string order;
            public string order_f;
            public string sc_ev;
            public string sc_inter;
            public string sh_comm_ev;
            public string show_video;
            public string st_ev;
            public string stat_link;
            public string status_ev;
            public string time_name;
            public string time_type;
            public string type;
            public string var;
            public string video_status;
            public string video_status2;
            public string video_type;
        }

        class Ev
        {
            public Dictionary<int, Data> Data { get; set; }
            public string Name; //Кто забьет гол
            public int Order;
            public int Type_site;
        }

        class Data
        {
            public Blocks Blocks;
            public int Order;
        }

        class Blocks
        {
            public T T;
        }

        public class T
        {
            public Tb Tb;
            public Tm Tm;
            public double Tot;
        }

        public class Tb
        {
            public double kf;
            public double lv;
            public double lvt;
            public string md;
            public string ps;
        }
        public class Tm
        {
            public double kf;
            public double lv;
            public double lvt;
            public string md;
            public string ps;
        }





        public List<SportEvent>  GetSportEvent(string page)
        {
            var result = new List<SportEvent>();
            //var page1 =  File.ReadAllText("fonbet.json");
            //var jsonData = JsonConvert.DeserializeObject<JsonData>(page);

            var m_res = JsonConvert.DeserializeObject<JsonData>(page);
           
            return null;
        }
    }
}
