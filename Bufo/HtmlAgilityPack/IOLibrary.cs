﻿// Decompiled with JetBrains decompiler
// Type: HtmlAgilityPack.IOLibrary
// Assembly: HtmlAgilityPack, Version=1.4.9.0, Culture=neutral, PublicKeyToken=bd319b19eaf3b43a
// MVID: 37A178A8-8F42-4354-B5E0-671855EB5B43
// Assembly location: F:\ShopRobotJap\packages\HtmlAgilityPack.1.4.9\lib\Net45\HtmlAgilityPack.dll

using System.IO;
using System.Runtime.InteropServices;

namespace HtmlAgilityPack
{
  [StructLayout(LayoutKind.Sequential, Size = 1)]
  internal struct IOLibrary
  {
    internal static void CopyAlways(string source, string target)
    {
      if (!File.Exists(source))
        return;
      Directory.CreateDirectory(Path.GetDirectoryName(target));
      IOLibrary.MakeWritable(target);
      File.Copy(source, target, true);
    }

    internal static void MakeWritable(string path)
    {
      if (!File.Exists(path))
        return;
      File.SetAttributes(path, File.GetAttributes(path) & ~FileAttributes.ReadOnly);
    }
  }
}
