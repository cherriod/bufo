﻿// Decompiled with JetBrains decompiler
// Type: HtmlAgilityPack.HtmlConsoleListener
// Assembly: HtmlAgilityPack, Version=1.4.9.0, Culture=neutral, PublicKeyToken=bd319b19eaf3b43a
// MVID: 37A178A8-8F42-4354-B5E0-671855EB5B43
// Assembly location: F:\ShopRobotJap\packages\HtmlAgilityPack.1.4.9\lib\Net45\HtmlAgilityPack.dll

using System;
using System.Diagnostics;

namespace HtmlAgilityPack
{
  internal class HtmlConsoleListener : TraceListener
  {
    public override void Write(string Message)
    {
      this.Write(Message, "");
    }

    public override void Write(string Message, string Category)
    {
      Console.Write("T:" + Category + ": " + Message);
    }

    public override void WriteLine(string Message)
    {
      this.Write(Message + "\n");
    }

    public override void WriteLine(string Message, string Category)
    {
      this.Write(Message + "\n", Category);
    }
  }
}
