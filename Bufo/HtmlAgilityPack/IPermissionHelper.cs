﻿// Decompiled with JetBrains decompiler
// Type: HtmlAgilityPack.IPermissionHelper
// Assembly: HtmlAgilityPack, Version=1.4.9.0, Culture=neutral, PublicKeyToken=bd319b19eaf3b43a
// MVID: 37A178A8-8F42-4354-B5E0-671855EB5B43
// Assembly location: F:\ShopRobotJap\packages\HtmlAgilityPack.1.4.9\lib\Net45\HtmlAgilityPack.dll

namespace HtmlAgilityPack
{
  /// <summary>
  /// An interface for getting permissions of the running application
  /// 
  /// </summary>
  public interface IPermissionHelper
  {
    /// <summary>
    /// Checks to see if Registry access is available to the caller
    /// 
    /// </summary>
    /// 
    /// <returns/>
    bool GetIsRegistryAvailable();

    /// <summary>
    /// Checks to see if DNS information is available to the caller
    /// 
    /// </summary>
    /// 
    /// <returns/>
    bool GetIsDnsAvailable();
  }
}
