﻿// Decompiled with JetBrains decompiler
// Type: HtmlAgilityPack.MixedCodeDocumentTextFragment
// Assembly: HtmlAgilityPack, Version=1.4.9.0, Culture=neutral, PublicKeyToken=bd319b19eaf3b43a
// MVID: 37A178A8-8F42-4354-B5E0-671855EB5B43
// Assembly location: F:\ShopRobotJap\packages\HtmlAgilityPack.1.4.9\lib\Net45\HtmlAgilityPack.dll

namespace HtmlAgilityPack
{
  /// <summary>
  /// Represents a fragment of text in a mixed code document.
  /// 
  /// </summary>
  public class MixedCodeDocumentTextFragment : MixedCodeDocumentFragment
  {
    /// <summary>
    /// Gets the fragment text.
    /// 
    /// </summary>
    public string Text
    {
      get
      {
        return this.FragmentText;
      }
      set
      {
        this.FragmentText = value;
      }
    }

    internal MixedCodeDocumentTextFragment(MixedCodeDocument doc)
      : base(doc, MixedCodeDocumentFragmentType.Text)
    {
    }
  }
}
