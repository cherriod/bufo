﻿// Decompiled with JetBrains decompiler
// Type: HtmlAgilityPack.EncodingFoundException
// Assembly: HtmlAgilityPack, Version=1.4.9.0, Culture=neutral, PublicKeyToken=bd319b19eaf3b43a
// MVID: 37A178A8-8F42-4354-B5E0-671855EB5B43
// Assembly location: F:\ShopRobotJap\packages\HtmlAgilityPack.1.4.9\lib\Net45\HtmlAgilityPack.dll

using System;
using System.Text;

namespace HtmlAgilityPack
{
  internal class EncodingFoundException : Exception
  {
    private Encoding _encoding;

    internal Encoding Encoding
    {
      get
      {
        return this._encoding;
      }
    }

    internal EncodingFoundException(Encoding encoding)
    {
      this._encoding = encoding;
    }
  }
}
