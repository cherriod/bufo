﻿// Decompiled with JetBrains decompiler
// Type: HtmlAgilityPack.HtmlWebException
// Assembly: HtmlAgilityPack, Version=1.4.9.0, Culture=neutral, PublicKeyToken=bd319b19eaf3b43a
// MVID: 37A178A8-8F42-4354-B5E0-671855EB5B43
// Assembly location: F:\ShopRobotJap\packages\HtmlAgilityPack.1.4.9\lib\Net45\HtmlAgilityPack.dll

using System;

namespace HtmlAgilityPack
{
  /// <summary>
  /// Represents an exception thrown by the HtmlWeb utility class.
  /// 
  /// </summary>
  public class HtmlWebException : Exception
  {
    /// <summary>
    /// Creates an instance of the HtmlWebException.
    /// 
    /// </summary>
    /// <param name="message">The exception's message.</param>
    public HtmlWebException(string message)
      : base(message)
    {
    }
  }
}
