﻿// Decompiled with JetBrains decompiler
// Type: HtmlAgilityPack.Utilities
// Assembly: HtmlAgilityPack, Version=1.4.9.0, Culture=neutral, PublicKeyToken=bd319b19eaf3b43a
// MVID: 37A178A8-8F42-4354-B5E0-671855EB5B43
// Assembly location: F:\ShopRobotJap\packages\HtmlAgilityPack.1.4.9\lib\Net45\HtmlAgilityPack.dll

using System.Collections.Generic;

namespace HtmlAgilityPack
{
  internal static class Utilities
  {
    public static TValue GetDictionaryValueOrNull<TKey, TValue>(Dictionary<TKey, TValue> dict, TKey key) where TKey : class
    {
      if (!dict.ContainsKey(key))
        return default (TValue);
      return dict[key];
    }
  }
}
