﻿// Decompiled with JetBrains decompiler
// Type: HtmlAgilityPack.AttributeValueQuote
// Assembly: HtmlAgilityPack, Version=1.4.9.0, Culture=neutral, PublicKeyToken=bd319b19eaf3b43a
// MVID: 37A178A8-8F42-4354-B5E0-671855EB5B43
// Assembly location: F:\ShopRobotJap\packages\HtmlAgilityPack.1.4.9\lib\Net45\HtmlAgilityPack.dll

namespace HtmlAgilityPack
{
  /// <summary>
  /// An Enum representing different types of Quotes used for surrounding attribute values
  /// 
  /// </summary>
  public enum AttributeValueQuote
  {
    SingleQuote,
    DoubleQuote,
  }
}
