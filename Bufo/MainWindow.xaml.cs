﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using WindowsInput;
using Bufo.Model;
using Bufo.ViewModel;
using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using HtmlDocument = HtmlAgilityPack.HtmlDocument;
using Path = System.IO.Path;

namespace Bufo
{
    public static class SSLValidator
    {
        private static bool OnValidateCertificate(object sender, X509Certificate certificate, X509Chain chain,
                                                  SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
        public static void OverrideValidation()
        {
            ServicePointManager.ServerCertificateValidationCallback =
                OnValidateCertificate;
            ServicePointManager.Expect100Continue = true;
        }
    }


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        [DllImport("user32.dll")]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        private double _filterPercentFrom;
        private double _filterPercentTo;

        public MainWindow()
        {
            InitializeComponent();
            ChkUseProxy.IsChecked = Properties.Settings.Default.ProxyUse;
            TextBoxProxyAddress.Text = Properties.Settings.Default.ProxyAddress;
            TextBoxProxyPort.Text = Properties.Settings.Default.ProxyPort;
            TextBoxRound.Text = Properties.Settings.Default.Round.ToString();
            EdtTotalAmount.Text = Properties.Settings.Default.Amount.ToString();
            TextBoxFilterFrom.Text = Properties.Settings.Default.FilterFrom.ToString();
            TextBoxFilterTo.Text = Properties.Settings.Default.FilterTo.ToString();

            _filterPercentFrom = Properties.Settings.Default.FilterFrom;
            _filterPercentTo = Properties.Settings.Default.FilterTo;
        }

        private async Task<string> ClickFind()
        {
            double totalAmount;
            double.TryParse(EdtTotalAmount.Text, out totalAmount);
            LblStatus.Content = "Работаю...";

            try
            {
                //LblFork.Content = await FillForkList();
                var forks = await FillForkList();
                
                TblForks.ItemsSource = forks;
                LblStatus.Content = "";
            }
            catch (Exception ex)
            {
                LblStatus.Content = ex.Message;
                //throw ex;
            }

            return "";
        }



        private bool _autoFindIsChecked = false;

        private async void ButtonAutoFind_Checked(object sender, RoutedEventArgs e)
        {
            _autoFindIsChecked = true;
            var x = await AutoFind();
        }

        private void ButtonAutoFind_Unchecked(object sender, RoutedEventArgs e)
        {
            _autoFindIsChecked = false;
        }

        private async Task<string> AutoFind()
        {
            string res = "";
            while (_autoFindIsChecked)
            {
                res = await ClickFind();
                await Task.Delay(1000);
            }
            return res;
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            string x = await ClickFind();
        }


        private void BrowserCommand()
        {
            Process[] procsChrome = Process.GetProcessesByName("chrome");
            foreach (Process chrome in procsChrome)
            {
                if (chrome.MainWindowHandle != IntPtr.Zero)
                {
                    // Set focus on the window so that the key input can be received.
                    SetForegroundWindow(chrome.MainWindowHandle);

                    //InputSimulator.SimulateKeyPress(VirtualKeyCode.SPACE);
                    InputSimulator.SimulateModifiedKeyStroke(VirtualKeyCode.CONTROL, VirtualKeyCode.VK_F);
                    InputSimulator.SimulateTextEntry("Example");

                }
            }
            
        }



        private void SetProxy()
        {
            if (ChkUseProxy.IsChecked == true && !string.IsNullOrEmpty(TextBoxProxyAddress.Text) &&
               !string.IsNullOrEmpty(TextBoxProxyPort.Text))
            {
                //bufoWebClient.UseProxy = true;
                //bufoWebClient.ProxyAddress = TextBoxProxyAddress.Text;
                var proxyPort = 0;
                Int32.TryParse(TextBoxProxyPort.Text, out proxyPort);
                //bufoWebClient.ProxyPort = proxyPort;
                BufoWebClient.SetProxy(TextBoxProxyAddress.Text, proxyPort);
            }
            else //bufoWebClient.UseProxy = false;
                BufoWebClient.UnsetProxy();
        }

    

        private void playSound()
        {
            System.Media.SoundPlayer player =
                new System.Media.SoundPlayer();
            player.SoundLocation = Path.Combine(Environment.CurrentDirectory, "shot.wav");
            player.Load();
            player.Play();
        }


        public async Task<List<ForkTableString>> FillForkList()
        {
            const string urlTennisi =
                @"https://tennisi.com/m28d/cgi/!book2_free.LiveBetsLines?val=1&gameid=5&categoryid=29010669&lang=rus&tbnohdr=1&rt=0.7174284971554779";
            const string urlFonbet = @"https://line.fbwebdn.com/live/currentLine/ru";
            const string urlBetcity = @"https://www.betcityrus.com/api/v1/live/bets?rev=6&ver=92&csn=f9xg7b";
            const string urlMarathon = @"https://www.marathonbet.com/su/live/popular";


            var bufoWebClient = new BufoWebClient();
            SetProxy();



            Task<string> tennisiTask = bufoWebClient.GetPageAsync(new Uri(urlTennisi));
            Task<string> fonbetTask = bufoWebClient.GetPageAsync(new Uri(urlFonbet), Encoding.UTF8);
            //Task<string> betcityTask = bufoWebClient.GetPageAsync(new Uri(urlBetcity), Encoding.UTF8);
            //Task<string> marathonTask = bufoWebClient.GetPageAsync(new Uri(urlMarathon), Encoding.UTF8);
           
            var pageTennisi = await tennisiTask;
            LblStatus.Content = "Получил тенниси...";
            var pageFonbet = await fonbetTask;
            LblStatus.Content = "Получил фонбет...";
            //var pageBetcity = await betcityTask;
            //LblStatus.Content = "Получил бетсити...";
            //var pageMarathon = await marathonTask;
            //LblStatus.Content = "Получил марафон...";

            var listFork = new List<ForkTableString>();
            var listTennisi = new BkTennisi().GetSportEvents(pageTennisi);
            var listFonbet = new BkFonbet().GetSportEvents(pageFonbet);
            //var listBetcity = new BkBetcity().GetSportEvent(pageBetcity);
            //var listMarathon = await new BkMarathon().GetSportEvents(pageMarathon);

            LblStatus.Content = "Ищу...";


            var tuple = GetForks(listFonbet, listTennisi);
           
          


           


            TblEvents.ItemsSource = tuple.Item1;
            listFork = tuple.Item2;

         /*   var myFork = new Fork(200, new Coefficient(BkEvent.Tm, 3, "//*"), new Coefficient(BkEvent.Tb, 4, "//*"),
                new Coefficient(BkEvent.Total, 25.5), new Coefficient(BkEvent.Total, 25.5));
            var sportEvent1 = new SportEvent()
            {
                BkName = "ten",
                Description = "Футбол 2 тайм",
                Player1 = "Спартак",
                Player2 = "Динамо"
            };
            var sportEvent2 = new SportEvent()
            {
                BkName = "fon",
                Description = "Футбол 2 тайм",
                Player1 = "Спартак",
                Player2 = "Динамо"
            };

            listFork.Add(new ForkTableString(myFork, sportEvent1, sportEvent2));*/

            return listFork;
        }


        private Tuple<List<EventTableString>, List<ForkTableString>> GetForks(List<SportEvent> sportList1, List<SportEvent> sportList2)
        {
            //var forks = new List<Fork>();
            var listEvents = new List<EventTableString>();
            var listFork = new List<ForkTableString>();

            foreach (var eventOfList1 in sportList1)
            {
                SportEvent sp2 = new SportEvent(){Player1= "", Player2 = ""};
                foreach (var eventOfList2 in sportList2)
                {
                    sp2 = eventOfList2;
                    
                    /////Фильтры здесь
                        //if (eventOfList1.AdditionalCef != null && eventOfList1.AdditionalCef.BkEventName == BkEvent.Total && eventOfList1.SportType != SportType.Football) continue;
                        //if (eventOfList2.AdditionalCef != null && eventOfList2.AdditionalCef.BkEventName == BkEvent.Total && eventOfList2.SportType != SportType.Football) continue;

                    /////
                     
                    
                    if (!ForkFinder.IsEqualSportEvents(eventOfList1, eventOfList2)) continue;

                    listEvents.Add(new EventTableString(eventOfList1, eventOfList2));

                    var totalAmount = 0d;
                    var round = 2;
                    double.TryParse(EdtTotalAmount.Text, out totalAmount);
                    int.TryParse(TextBoxRound.Text, out round);
                    var forkFinder = new ForkFinder(eventOfList1, eventOfList2, totalAmount, round);
                    var forks = forkFinder.Forks;
                    var _isForkExist = false;

                    //if (forks != null && forks.Count>0) listFork.AddRange(forks);
                    foreach (var fork in forks.Where(fork => fork != null && fork.ProfitPercent >= _filterPercentFrom && fork.ProfitPercent <= _filterPercentTo))
                    {
                        listFork.Add(new ForkTableString(fork, eventOfList1, eventOfList2));
                        _isForkExist = true;

                        Log.ToFile(DateTime.Now.ToString(new CultureInfo("ru-Ru")) +
                                   " Events: " + eventOfList1.Description + " " + eventOfList1.PartOfMatchName +
                                   " Amounts: " + fork.AmountPlayer1 + " " + fork.AmountPlayer2 +
                                   " BkNames: " + eventOfList1.BkName + " " + eventOfList2.BkName +
                                   " Coefficients: " + fork.Coefficient1 + " " + fork.Coefficient2 +
                                   " BkEvents: " + fork.Coefficient1.BkEventName + " " +
                                   (fork.AdditionalCef1 == null ? "" : fork.AdditionalCef1.Value.ToString()) +
                                   " " + fork.Coefficient2.BkEventName + " " +
                                   (fork.AdditionalCef2 == null ? "" : fork.AdditionalCef2.Value.ToString()) +
                                   " Players: " + eventOfList1.Player1 + " - " + eventOfList1.Player2 + " " +
                                   eventOfList2.Player1 + " - " + eventOfList2.Player2 +
                                   " ProfitPercent: " + fork.ProfitPercent.ToString() + " Profit1: " + fork.Profit1 +
                                   " Profit2: " + fork.Profit2);
                    }

                    if (_isForkExist) playSound();
                }
            }
            return new Tuple<List<EventTableString>, List<ForkTableString>>(listEvents, listFork);
        }

        private void ChkUseProxy_Checked(object sender, RoutedEventArgs e)
        {
            TextBoxProxyAddress.IsEnabled = true;
            TextBoxProxyPort.IsEnabled = true;
        }

        private void ChkUseProxy_Unchecked(object sender, RoutedEventArgs e)
        {
            TextBoxProxyAddress.IsEnabled = false;
            TextBoxProxyPort.IsEnabled = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (ChkUseProxy.IsChecked == true)
            {
                TextBoxProxyAddress.IsEnabled = true;
                TextBoxProxyPort.IsEnabled = true;
            }
            else
            {
                TextBoxProxyAddress.IsEnabled = false;
                TextBoxProxyPort.IsEnabled = false;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Properties.Settings.Default.ProxyUse = ChkUseProxy.IsChecked == true;
            Properties.Settings.Default.ProxyAddress = TextBoxProxyAddress.Text;
            Properties.Settings.Default.ProxyPort = TextBoxProxyPort.Text;
            var round = 0;
            var amount = 0;
            var filterFrom = 0d;
            var filterTo = 0d;
            int.TryParse(TextBoxRound.Text, out round);
            int.TryParse(EdtTotalAmount.Text, out amount);
            double.TryParse(TextBoxFilterFrom.Text, out filterFrom);
            double.TryParse(TextBoxFilterTo.Text, out filterTo);
            Properties.Settings.Default.Round = round;
            Properties.Settings.Default.Amount = amount;
            Properties.Settings.Default.FilterFrom = filterFrom;
            Properties.Settings.Default.FilterTo = filterTo;
            Properties.Settings.Default.Save();
        }

        private void TblForks_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            DependencyObject dep = (DependencyObject) e.OriginalSource;

            // iteratively traverse the visual tree
            while ((dep != null) &&
                   !(dep is DataGridCell) &&
                   !(dep is DataGridColumnHeader))
            {
                dep = VisualTreeHelper.GetParent(dep);
            }

            if (dep == null)
                return;

            if (dep is DataGridColumnHeader)
            {
                DataGridColumnHeader columnHeader = dep as DataGridColumnHeader;
                // do something
            }

            if (dep is DataGridCell)
            {
                DataGridCell cell = dep as DataGridCell;

                // navigate further up the tree
                while ((dep != null) && !(dep is DataGridRow))
                {
                    dep = VisualTreeHelper.GetParent(dep);
                }

                DataGridRow row = dep as DataGridRow;
                System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
                customCulture.NumberFormat.NumberDecimalSeparator = ".";

                //if (cell.Column.DisplayIndex == 8) GoToChrome(1, ((ForkTableString)row.Item).SportEvent1.Player1, ((ForkTableString)row.Item).SportEvent1.AdditionalCef.Value.ToString(customCulture), ((ForkTableString)row.Item).Xpath1);
                //if (cell.Column.DisplayIndex == 9) GoToChrome(2, ((ForkTableString)row.Item).SportEvent2.Player1, ((ForkTableString)row.Item).SportEvent2.AdditionalCef.Value.ToString(customCulture), ((ForkTableString)row.Item).Xpath2);
                
                if (cell.Column.DisplayIndex == 8 || cell.Column.DisplayIndex == 9)
                {
                    SaveTennisiMacros(@"C:\Users\Oleg\Documents\iMacros\Macros\tennisi.iim", ((ForkTableString)row.Item).Xpath1, ((ForkTableString)row.Item).Fork.AmountPlayer1);
                    SaveFonbetMacros(@"C:\Users\Oleg\Documents\iMacros\Macros\fonbet.iim", ((ForkTableString)row.Item).Xpath2, ((ForkTableString)row.Item).Fork.AmountPlayer2);
                    //TennisiFonbetMacros(@"C:\Users\Oleg\Documents\iMacros\Macros\tenfon.iim", ((ForkTableString)row.Item).Xpath1, ((ForkTableString)row.Item).Xpath2, ((ForkTableString)row.Item).Fork.AmountPlayer1, ((ForkTableString)row.Item).Fork.AmountPlayer2);
                    GoToFirefox(((ForkTableString)row.Item).Fork.Coefficient1.Value, ((ForkTableString)row.Item).Fork.Coefficient2.Value);
                }

                
            }
        }

        private void SaveTennisiMacros(string pathToFile, string xpath, double amount)
        {
            var lines = new List<string>()
            {
                //"TAB T=1",
                "VERSION BUILD=844 RECORDER=CR",
                "FRAME F=6",
                "EVENT TYPE=CLICK XPATH=\"" + xpath + "\" BUTTON=0",
                "TAG POS=1 TYPE=INPUT:TEXT FORM=ACTION:!BOOK2.BasketEdit ATTR=ID:singsum CONTENT=" + amount.ToString(),
                "EVENT TYPE=KEYPRESS SELECTOR=\"HTML>BODY>DIV:nth-of-type(4)>FORM>TABLE>TBODY>TR>TD>INPUT\" KEY=13"
            };

            File.WriteAllLines(pathToFile, lines);
        }

        private void SaveFonbetMacros(string pathToFile, string xpath, double amount)
        {
            var lines = new List<string>()
            {
                //"TAB T=2",
                "VERSION BUILD=844 RECORDER=CR",
                "EVENT TYPE=CLICK XPATH=\"" + xpath + "\" BUTTON=0",
                "TAG POS=1 TYPE=INPUT:TEXT ATTR=ID:couponNewSumEdit CONTENT=" + amount.ToString()
            };

            File.WriteAllLines(pathToFile, lines);
        }

        private void TennisiFonbetMacros(string pathToFile, string xpath1, string xpath2, double amount1, double amount2)
        {
            var lines = new List<string>()
            {
                "VERSION BUILD=844 RECORDER=CR",
                "TAB T=1",
                "FRAME F=6",
                "TAG XPATH=\"" + xpath1 + "\"",
                "TAG POS=1 TYPE=INPUT:TEXT FORM=ACTION:!BOOK2.BasketEdit ATTR=ID:singsum CONTENT=" + amount1.ToString(),

                "TAB T=2",
                 "EVENT TYPE=CLICK XPATH=\"" + xpath2 + "\" BUTTON=0",
                "TAG POS=1 TYPE=INPUT:TEXT ATTR=ID:couponNewSumEdit CONTENT=" + amount2.ToString()
            };
            File.WriteAllLines(pathToFile, lines);
        }

        private void GoToFirefox(double value1, double value2)
        {
            VirtualKeyCode firstOpenedTab;
            VirtualKeyCode secondOpenedTab;

            if (value1 <= value2)
            {
                firstOpenedTab = VirtualKeyCode.NUMPAD1;
                secondOpenedTab = VirtualKeyCode.NUMPAD2;
            }
            else
            {
                firstOpenedTab = VirtualKeyCode.NUMPAD2;
                secondOpenedTab = VirtualKeyCode.NUMPAD1;
            }

            Process[] processes = Process.GetProcessesByName("firefox");

            foreach (Process proc in processes)
            {
                if (proc.MainWindowHandle != IntPtr.Zero)
                {
                    // Set focus on the window so that the key input can be received.
                    SetForegroundWindow(proc.MainWindowHandle);

                   Thread.Sleep(200);
                   InputSimulator.SimulateModifiedKeyStroke(VirtualKeyCode.CONTROL, firstOpenedTab);
                   Thread.Sleep(200);
                   InputSimulator.SimulateModifiedKeyStroke(VirtualKeyCode.MENU, firstOpenedTab);
                   Thread.Sleep(200);
                   InputSimulator.SimulateModifiedKeyStroke(VirtualKeyCode.CONTROL, secondOpenedTab);
                   Thread.Sleep(200);
                   InputSimulator.SimulateModifiedKeyStroke(VirtualKeyCode.MENU, secondOpenedTab);
                }
            }
        }

        private void GoToChrome(int tab, string prefindText, string findText, string xpath)
        {

            By by = By.XPath(xpath);

            Clipboard.SetText(findText);
            Process[] procsChrome = Process.GetProcessesByName("chrome");
            foreach (Process chrome in procsChrome)
            {
                if (chrome.MainWindowHandle != IntPtr.Zero)
                {
                    // Set focus on the window so that the key input can be received.
                    SetForegroundWindow(chrome.MainWindowHandle);

                    //InputSimulator.SimulateKeyPress(VirtualKeyCode.SPACE);
                    if (tab == 1)
                        InputSimulator.SimulateModifiedKeyStroke(VirtualKeyCode.CONTROL, VirtualKeyCode.NUMPAD1);
                    else if (tab == 2)
                        InputSimulator.SimulateModifiedKeyStroke(VirtualKeyCode.CONTROL, VirtualKeyCode.NUMPAD2);
                    else return;

                    Thread.Sleep(100);

                    
                    using (IWebDriver driver = new ChromeDriver())
                    {
                        //var handle = driver.CurrentWindowHandle;
                        var locator = driver.SwitchTo().Window("chrome").SwitchTo().Frame("BetsList");

                        //driver.
                        //Notice navigation is slightly different than the Java version
                        //This is because 'get' is a keyword in C#
                        
                        locator.FindElement(By.XPath(xpath)).Click();

                        // Find the text input element by its name
                    }



                  /*  Thread.Sleep(100);
                    InputSimulator.SimulateModifiedKeyStroke(VirtualKeyCode.CONTROL, VirtualKeyCode.VK_F);
                    Thread.Sleep(20);

                
                    InputSimulator.SimulateTextEntry(prefindText);

                    Thread.Sleep(20);
                    InputSimulator.SimulateModifiedKeyStroke(VirtualKeyCode.CONTROL, VirtualKeyCode.VK_F);
                    InputSimulator.SimulateTextEntry(findText);*/


                    //InputSimulator.SimulateModifiedKeyStroke(VirtualKeyCode.CONTROL, VirtualKeyCode.VK_V);
                    //InputSimulator.SimulateKeyPress(VirtualKeyCode.RETURN);

                }
            }
        }

        private int FindRowIndex(DataGridRow row)
        {
            DataGrid dataGrid =
                ItemsControl.ItemsControlFromItemContainer(row)
                as DataGrid;

            int index = dataGrid.ItemContainerGenerator.
                IndexFromContainer(row);

            return index;
        }

       

       

        private void TextBoxFilterFrom_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                _filterPercentFrom = double.Parse(TextBoxFilterFrom.Text, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                _filterPercentFrom = 0;
                TextBoxFilterFrom.Text = "0";
            }
        }

        private void TextBoxFilterTo_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                _filterPercentTo = double.Parse(TextBoxFilterTo.Text, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture);
            }
            catch (Exception)
            {
                _filterPercentTo = 100;
                TextBoxFilterTo.Text = "100";
            }

        }


        private void AlterRow(DataGridRowEventArgs e)
        {
            var cell = GetCell(TblForks, e.Row, 5);
            if (cell == null) return;

            var item = e.Row.Item as ForkTableString;
            if (item == null) return;

            cell.Background = Brushes.Red;
            
        }

        public static DataGridRow GetRow(DataGrid grid, int index)
        {
            var row = grid.ItemContainerGenerator.ContainerFromIndex(index) as DataGridRow;

            if (row == null)
            {
                // may be virtualized, bring into view and try again
                grid.ScrollIntoView(grid.Items[index]);
                row = (DataGridRow)grid.ItemContainerGenerator.ContainerFromIndex(index);
            }
            return row;
        }

        public static T GetVisualChild<T>(Visual parent) where T : Visual
        {
            T child = default(T);
            int numVisuals = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < numVisuals; i++)
            {
                var v = (Visual)VisualTreeHelper.GetChild(parent, i);
                child = v as T ?? GetVisualChild<T>(v);
                if (child != null)
                {
                    break;
                }
            }
            return child;
        }

        public static DataGridCell GetCell(DataGrid host, DataGridRow row, int columnIndex)
        {
            if (row == null) return null;

            var presenter = GetVisualChild<DataGridCellsPresenter>(row);
            if (presenter == null) return null;

            // try to get the cell but it may possibly be virtualized
            var cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(columnIndex);
            if (cell == null)
            {
                // now try to bring into view and retreive the cell
                host.ScrollIntoView(row, host.Columns[columnIndex]);
                cell = (DataGridCell)presenter.ItemContainerGenerator.ContainerFromIndex(columnIndex);
            }
            return cell;

        }

        private void TblForks_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            Dispatcher.BeginInvoke(DispatcherPriority.Render, new Action(() => AlterRow(e)));
        }



    }

   
}
    

